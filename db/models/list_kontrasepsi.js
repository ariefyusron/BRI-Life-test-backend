'use strict';
module.exports = (sequelize, DataTypes) => {
  const LIST_KONTRASEPSI = sequelize.define('LIST_KONTRASEPSI', {
    Nama_Kontrasepsi: DataTypes.STRING
  }, {});
  LIST_KONTRASEPSI.associate = function(models) {
    // associations can be defined here
  };
  return LIST_KONTRASEPSI;
};