'use strict';
module.exports = (sequelize, DataTypes) => {
  const LIST_PROPINSI = sequelize.define('LIST_PROPINSI', {
    Nama_Propinsi: DataTypes.STRING
  }, {});
  LIST_PROPINSI.associate = function(models) {
    // associations can be defined here
  };
  return LIST_PROPINSI;
};