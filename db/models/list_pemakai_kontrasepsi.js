'use strict';
module.exports = (sequelize, DataTypes) => {
  const LIST_PEMAKAI_KONTRASEPSI = sequelize.define('LIST_PEMAKAI_KONTRASEPSI', {
    Id_Propinsi: DataTypes.INTEGER,
    Id_Kontrasepsi: DataTypes.INTEGER,
    Jumlah_Pemakai: DataTypes.INTEGER
  }, {});
  LIST_PEMAKAI_KONTRASEPSI.associate = function(models) {
    // associations can be defined here
    LIST_PEMAKAI_KONTRASEPSI.belongsTo(models.LIST_PROPINSI, {
      foreignKey: 'Id_Propinsi',
      as: 'propinsi'
    })
    LIST_PEMAKAI_KONTRASEPSI.belongsTo(models.LIST_KONTRASEPSI, {
      foreignKey: 'Id_Kontrasepsi',
      as: 'kontrasepsi'
    })
  };
  return LIST_PEMAKAI_KONTRASEPSI;
};