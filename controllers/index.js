const models = require('../db/models');

exports.index = async (req,res) => {
  const listPemakai = await models.LIST_PEMAKAI_KONTRASEPSI.findAll({
    include: [{
      model: models.LIST_PROPINSI,
      as: 'propinsi'
    },{
      model: models.LIST_KONTRASEPSI,
      as: 'kontrasepsi'
    }],
    order: [['id','DESC']]
  })
  res.json(listPemakai)
}

exports.store = async (req,res) => {
  const store = await models.LIST_PEMAKAI_KONTRASEPSI.create(req.body)
  const data = await models.LIST_PEMAKAI_KONTRASEPSI.find({
    where: {
      id: store.id
    },
    include: [{
      model: models.LIST_PROPINSI,
      as: 'propinsi'
    },{
      model: models.LIST_KONTRASEPSI,
      as: 'kontrasepsi'
    }],
    order: [['id','DESC']]
  })
  res.json(data)
}