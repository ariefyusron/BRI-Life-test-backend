const models = require('../db/models');

exports.index = async (req,res) => {
  const propinsi = await models.sequelize.query('SELECT id, Nama_Propinsi from LIST_PROPINSIs;')
  for(i in propinsi[0]){
    const jumlahPemakai = await models.sequelize.query(`select JumlahPemakai from LIST_PROPINSIs as a join (select Id_Propinsi, sum(Jumlah_Pemakai) as JumlahPemakai from LIST_PEMAKAI_KONTRASEPSIs where Id_Propinsi=${propinsi[0][i].id}) as b where a.id = b.Id_Propinsi;`)
    const detailPemakai = await models.sequelize.query(`select b.id, Id_Propinsi, Nama_Propinsi, Id_Kontrasepsi, Nama_Kontrasepsi ,Jumlah from LIST_PROPINSIs as a join (select id, Id_Propinsi,Id_Kontrasepsi,sum(Jumlah_Pemakai) as Jumlah from LIST_PEMAKAI_KONTRASEPSIs where Id_Propinsi=${propinsi[0][i].id} group by Id_Kontrasepsi) as b join LIST_KONTRASEPSIs as c where a.id=b.Id_Propinsi and b.Id_Kontrasepsi=c.id;`)
    propinsi[0][i].jumlahPemakai = jumlahPemakai[0][0]? parseInt(jumlahPemakai[0][0].JumlahPemakai):0
    propinsi[0][i].detailPemakai = detailPemakai[0]
  }
  res.json(propinsi[0])
}