const models = require('../db/models');

exports.index = async (req,res) => {
  const list = await models.LIST_KONTRASEPSI.findAll()
  res.json(list)
}

exports.store = async (req,res) => {
  const store = await models.LIST_KONTRASEPSI.create(req.body)
  res.json(store)
}