require('dotenv').config();
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser');
const validator = require('express-validator');

const indexRouter = require('./routes/index');
const propinsiRouter = require('./routes/propinsi');
const kontrasepsiRouter = require('./routes/kontrasepsi');
const reportRouter = require('./routes/report');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(validator());

app.use('/', indexRouter);
app.use('/propinsi', propinsiRouter);
app.use('/kontrasepsi', kontrasepsiRouter);
app.use('/report', reportRouter)

module.exports = app;
