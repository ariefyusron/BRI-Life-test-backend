'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('LIST_PEMAKAI_KONTRASEPSIs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      Id_Propinsi: {
        type: Sequelize.INTEGER,
        references: {
          model: 'LIST_PROPINSIs',
          key: 'id'
        }
      },
      Id_Kontrasepsi: {
        type: Sequelize.INTEGER,
        references: {
          model: 'LIST_KONTRASEPSIs',
          key: 'id'
        }
      },
      Jumlah_Pemakai: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('LIST_PEMAKAI_KONTRASEPSIs');
  }
};