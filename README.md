## Setup

Clone the repo `git clone https://github.com/ariefyusron/BRI-Life-test-backend.git <project_name>`

Open directory `cd <project_name>`

and then run `npm install`


## Start Server

```js
npm start
```

nb: don't forget run mysql


## Migration

Running `npm run migrate`