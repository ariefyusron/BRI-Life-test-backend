const models = require('../db/models');

exports.index = async (req,res) => {
  const list = await models.LIST_PROPINSI.findAll()
  res.json(list)
}

exports.store = async (req,res) => {
  const store = await models.LIST_PROPINSI.create(req.body)
  res.json(store)
}